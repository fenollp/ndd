#!/usr/bin/env escript

-mode(compile).
%% http://paste.awesom.eu/radioxid/bAt&ln

main([]) -> usage();
main(Arg) ->
    [ok,ok,ok,ok,ok] = [application:start(X) || X <- [ crypto, asn1
                                                     , public_key
                                                     , ssl,    inets]],
    L = map_reduce_timed(fun check/1, Arg),
    retry(L, []).

retry([], []) -> ok;
retry([], L) -> map_reduce_timed(fun check/1, L);
retry([R|T], Ndds) ->
    case R of {_,{error,Ndd}} -> retry(T, [Ndd|Ndds]); _ -> retry(T, Ndds) end.

e(Ndd, L) -> io:format(standard_error, "ERROR: [~s] ~p\n", [Ndd,L]).
s(M) -> io:format(M ++"\n", []).
out(Ndd, Verdict) -> s("➤ "++ Ndd ++"\t"++ Verdict).
usage() -> io:format(standard_error, "Usage: ./… ‹domain names to check›\n", []).

check(Ndd) ->
    Hidden = "&domain_domainChooser_domainmode=1mainDomain&domain_domainChooser_stayHere=0",
    Query = "domain_domainChooser_domain="++ Ndd ++ Hidden,
    Url = "https://www.ovh.com/fr/cgi-bin/newOrder/order.cgi",
    UA = "Mozilla/4.0 (compatible; Opera/3.0; Windows 4.10) 3.51",
    Content_type = "application/x-www-form-urlencoded",
    case httpc:request(post, {Url, [{"user-agent",UA}], Content_type, Query}, [], []) of
    {ok,{_,_,Content}} ->
	    case regex("existe déjà,", Content) of
	    {match,_} ->
		    out(Ndd, "Taken"), {taken,Ndd};
	    _ ->
		    case regex("disponible", Content) of
		    {match,_} ->
			    out(Ndd, "Yay FREE!!!1"), {free,Ndd};
		    _ ->
			e(Ndd,"RegEx no longer suited"), {no_retry_then,Ndd}
		    end
	    end;
    Otherwise ->
	    e(Ndd,Otherwise), {error,Ndd}
    end.



%%% unix pipe-ing
%      case io:get_chars('', 8192) of
%	      eof ->
%	      init:stop();
%	  Text ->    Rotated = [rot13(C) || C <- Text],
%		         io:put_chars(Rotated),
%		         rot13()
%			 end.
%
%After compiling this, you can run it from the command line:
%matthias> cat ~/.bashrc | erl -noshell -s rot13 rot13 | wc




regex(Pat, Subj) -> regex(Pat, Subj, []).
regex(Pat, Subj, Options) ->
  re:run(Subj, Pat, Options ++ [{capture, all_but_first, list}, global,
    dollar_endonly, unicode, dotall]).


report(_, Msg, List) -> io:format("~p ~p~n", [Msg, List]).

%%% time_slaves/1
%% Like spawn_slaves/1, but returns µtimed tuples
%% Returns :: [{Time, Result}, …] | [{Time, Error}, …]
time_slaves([]) -> {error, nothing_passed};
time_slaves(A) ->
  spawn_slaves(A, self(), length(A), erlang:make_ref(), fun run_timed/4).
%%%

spawn_slaves([], _, Len, Ref, _) -> wait_for_them([], Len, Ref);
spawn_slaves(L=[Tuple|Tail], Spawner, Len, Ref, Run) ->
  Index = 1 + Len - length(L),
  spawn(fun() -> Run(Spawner, Ref, Index, Tuple) end), %% can't fail
  spawn_slaves(Tail, Spawner, Len, Ref, Run).


run_timed(P, R, I, {N, F, A}) ->
    Self = self(),
    process_flag(trap_exit, true),
      spawn_link(fun() ->
			 Self ! {res, timer:tc( fun() -> F(A) end )} end),
      %% Gives up {res, {µsecs, Res}} !!
  wait_for_one(P, R, I, N, A).


wait_for_one(Parent, Ref, Index, ReadableName, Arg) ->
  receive
  {res, Res} -> Parent ! {Ref, Index, {ReadableName, Res}};
  {'EXIT', _, normal} ->
    wait_for_one(Parent, Ref, Index, ReadableName, Arg);
    %% and we should receive a {res, Res} now.
  {'EXIT', _, Reason} ->
    report(locally, "Trapped 'EXIT' !!", [{name,ReadableName}, {arg,Arg}, 
      {reason,Reason}]),
    Parent ! {Ref, Index, {ReadableName, Reason}}
  end.

wait_for_them(List, 0, _) -> order_list(List, 1);
wait_for_them(List, N, Ref) ->
  receive
  {Ref, Index, Res} -> wait_for_them([{Index,Res}|List], N -1, Ref)
  end.

order_list([], _) -> [];
order_list(List, Index) ->
  [Var] = browse_for_lefthandside(List, [Index]),
  [Var|order_list(proplists:delete(Index, List), Index +1)].
%%%


%%% browse_for_lefthandside/2
% proplists:get_value/2 ?
browse_for_lefthandside(_, []) -> [];
browse_for_lefthandside(Tuples, [Pattern|T]) ->
  [find_value_from_key(Pattern, Tuples) | browse_for_lefthandside(Tuples, T)].
find_value_from_key(Pattern, [{Pattern,Treasure} |_]) -> Treasure;
find_value_from_key(Pattern, [_|Ts]) -> find_value_from_key(Pattern, Ts);
find_value_from_key(_, []) -> none.
%%%

%%% map_reduce_timed/2
map_reduce_timed(A, B) when is_list(A) and is_list(B) ->
  browse_for_lefthandside(time_slaves(A), B);
%% Carefull. Don't pass a list as Funs' argument
map_reduce_timed(A, B) ->
  merge_righthandsides(time_slaves(recur(A, B))).
%%%

%%% merge_righthandsides/1
merge_righthandsides([]) -> [];
merge_righthandsides([{_,R}|T]) -> [R|merge_righthandsides(T)].
%%%

%%% recur/3
% truth table seems clunky; moreover we might want to apply Funs to Arg 
%   when Arg is a list as well
recur([], []) -> [];
recur(A, []) when not is_list(A) -> [];  %% Can't pass an empty list to Funs.
recur([], B) when not is_list(B) -> [];
recur(Fun, Args) when is_list(Args) ->
  [Arg|ArgList] = Args,
  %% Starts from length(ArgList) +1, ends with 1. Counting top to bottom.
  [{length(ArgList) +1, Fun, Arg} | recur(Fun, ArgList)];
recur(Funs, Arg) when is_list(Funs) ->
  [Fun|FunList] = Funs,
  [{length(FunList) +1, Fun, Arg} | recur(FunList, Arg)].
%%%

